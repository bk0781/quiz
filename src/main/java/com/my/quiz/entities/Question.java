package com.my.quiz.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Setter
    private String text;

    @Setter
    private Boolean active;

    @OneToMany(mappedBy = "question", fetch = FetchType.LAZY)
    private List<Answer> answers;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @Setter
    private Category category;

    public Question(String text, Category category) {
        this.text = text;
        this.category = category;
        this.active = true;
    }

    public Question(int id) {
        this.id = id;
        this.active = true;
    }
}
