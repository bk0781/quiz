package com.my.quiz.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Setter
    String name;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    private List<Question> questions;

    //TODO лютый говнокод
    @Setter
    @Transient
    private String someClass = "";

    public Category(String name){
        this.name = name;
    }
}
