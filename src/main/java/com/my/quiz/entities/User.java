package com.my.quiz.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String studyGroup;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Answer> answers;

    public User(String name, String studyGroup) {
        this.name = name;
        this.studyGroup = studyGroup;
    }
}