package com.my.quiz.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
public class RawAnswer {
    @NotBlank
    private String answer;

    @Positive
    private int questionId;
}
