package com.my.quiz.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@AllArgsConstructor
public class QuizDTO {
    @NotBlank
    private String userId;

    @NotEmpty
    private List<RawAnswer> rawAnswers;
}
