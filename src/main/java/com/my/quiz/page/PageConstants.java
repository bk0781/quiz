package com.my.quiz.page;

public class PageConstants {
    public static final String MAIN_PAGE = "/main";
    public static final String QUIZ_PAGE = "/quiz";
    public static final String SETUP_PAGE = "/setup";
    public static final String END_PAGE = "/end";
    public static final String STAT_PAGE = "/stat";
    public static final String GREET_PAGE = "/greet";
    public static final String CATEGORY_PAGE = "/category";
}
