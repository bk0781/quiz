package com.my.quiz.repository;

import com.my.quiz.entities.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    Question findById (int id);

    List<Question> findByCategoryId(int categoryId);

    List<Question> findByCategoryIdAndActiveTrue(int categoryId);

    List<Question> findByActiveTrue();
}
