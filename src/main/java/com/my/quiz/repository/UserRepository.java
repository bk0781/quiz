package com.my.quiz.repository;

import com.my.quiz.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findById(int id);
    User findFirsByNameAndStudyGroup(String name, String studyGroup);
}
