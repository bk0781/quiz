package com.my.quiz.repository;

import com.my.quiz.entities.Category;
import com.my.quiz.entities.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Category findById(int id);
    Category findFirstByNameIgnoreCase(String name);
}
