package com.my.quiz.repository;

import com.my.quiz.entities.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
    List<Answer> findByUserId(int userId);

    List<Answer> findByQuestionId(int questionId);

    @Query("select a from Answer a join fetch a.question q join fetch q.category c")
    List<Answer> findAllAndFetch();
}
