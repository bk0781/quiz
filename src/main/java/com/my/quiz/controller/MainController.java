package com.my.quiz.controller;

import com.my.quiz.dto.*;
import com.my.quiz.entities.*;
import com.my.quiz.page.PageConstants;
import com.my.quiz.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
public class MainController {
    private final QuestionService questionService;
    private final AnswerService answerService;
    private final UserService userService;
    private final CategoryService categoryService;

    public MainController(QuestionService questionService, AnswerService answerService, UserService userService,
                          CategoryService categoryService) {
        this.questionService = questionService;
        this.answerService = answerService;
        this.userService = userService;
        this.categoryService = categoryService;
    }

    @GetMapping
    public String getMainPage(Map<String, Object> model){
        return PageConstants.MAIN_PAGE;
    }

    @GetMapping("greet")
    public String getGreetPage(Map<String, Object> model){
        return PageConstants.GREET_PAGE;
    }

    @PostMapping("greet")
    @ResponseBody
    public String saveUser(@Validated @RequestBody User user, Map<String, Object> model){
        User existingUser = userService.findFirstByNameAndStudyGroup(user.getName(), user.getStudyGroup());

        if (existingUser != null){
            return "/quiz?userId=" + existingUser.getId();
        }

        User newUser = userService.save(user.getName(), user.getStudyGroup());
        return "/quiz?userId=" + newUser.getId();
    }

    @GetMapping("quiz")
    public String getCategoryPage(@RequestParam int userId, Map<String, Object> model){
        List<Category> categories = categoryService.getAllCategories();
        model.put("categories", categories);
        model.put("userId", userId);
        return PageConstants.CATEGORY_PAGE;
    }

    @GetMapping("quiz/{categoryId}")
    public String getQuizPage(@RequestParam int userId, @PathVariable int categoryId, Map<String, Object> model){
        List<Question> questions = questionService.findByCategoryIdAndActiveTrue(categoryId);
        model.put("questions", questions);
        model.put("userId", userId);
        return PageConstants.QUIZ_PAGE;
    }

    @PostMapping("quiz/{categoryId}")
    @ResponseBody
    public String saveAnswer(@Validated @RequestBody QuizDTO quizDTO, @PathVariable int categoryId,
                             Map<String, Object> model){
        //TODO Не атамарное сохранение, возможен вариант, когда сохранился user, но ответы не сохранились (ошибка какая-то, например). Можно пока забить
        User user = userService.findById(Integer.parseInt(quizDTO.getUserId()));
        answerService.save(quizDTO.getRawAnswers(), user);
        return "/quiz?userId=" + user.getId();
    }

    @GetMapping("setup")
    public String getSetupPage(Map<String, Object> model){
        List<Question> questions = questionService.getAllQuestions();
        model.put("questions", questions);
        List<Category> categories = categoryService.getAllCategories();
        model.put("categories", categories);
        return PageConstants.SETUP_PAGE;
    }

    @PostMapping("addCategory")
    public String saveCategory(@RequestParam String categoryName, Map<String, Object> model){
        Category existingCategory = categoryService.findFirstByName(categoryName);
        if (existingCategory == null){
            categoryService.save(categoryName);
        }

        List<Question> questions = questionService.getAllQuestions();
        model.put("questions", questions);

        List<Category> categories = categoryService.getAllCategories();
        model.put("categories", categories);
        return PageConstants.SETUP_PAGE;
    }

    @PostMapping("editCategory")
    public String editQuestion(@RequestParam int categoryId, @RequestParam String name, Map<String, Object> model){
        Category category = categoryService.findById(categoryId);

        category.setName(name);

        List<Question> questions = questionService.getAllQuestions();
        model.put("questions", questions);

        List<Category> categories = categoryService.getAllCategories();
        model.put("categories", categories);
        return PageConstants.SETUP_PAGE;
    }

    @PostMapping("addQuestion")
    public String saveQuestion(@RequestParam String text, @RequestParam Integer categoryId, Map<String, Object> model){
        Category category = categoryService.findById(categoryId);

        questionService.save(text, category);

        List<Question> questions = questionService.getAllQuestions();
        model.put("questions", questions);

        List<Category> categories = categoryService.getAllCategories();
        model.put("categories", categories);
        return PageConstants.SETUP_PAGE;
    }


    @PostMapping("editQuestion")
    public String editQuestion(@RequestParam int questionId, @RequestParam (required = false) Integer categoryId,
                               @RequestParam String text, @RequestParam (required = false) Boolean isActive, Map<String, Object> model){
        Question question = questionService.findById(questionId);

        if (categoryId != null) {
            Category category = categoryService.findById(categoryId);

            if (category != null) {
                question.setCategory(category);
            }
        }

        question.setText(text);
        if (isActive != null){
            question.setActive(isActive);
        }

        List<Question> questions = questionService.getAllQuestions();
        model.put("questions", questions);

        List<Category> categories = categoryService.getAllCategories();
        model.put("categories", categories);
        return PageConstants.SETUP_PAGE;
    }

    @GetMapping("stat")
    public String getStatPage(Map<String, Object> model){
        List<Question> questions = questionService.getAllQuestions();

        List<User> users = questions.stream().map(Question::getAnswers)
                .flatMap(Collection::stream)
                .map(Answer::getUser)
                .distinct()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        model.put("users", users);

        List<Category> categories = questions.stream()
                .map(Question::getCategory)
                .distinct()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(categories)) { categories.get(0).setSomeClass("is-active"); }

        model.put("categories", categories);
        return PageConstants.STAT_PAGE;
    }
}
