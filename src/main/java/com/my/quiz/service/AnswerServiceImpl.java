package com.my.quiz.service;

import com.my.quiz.entities.*;
import com.my.quiz.dto.RawAnswer;
import com.my.quiz.repository.AnswerRepository;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AnswerServiceImpl implements AnswerService {
    private final AnswerRepository answerRepository;

    public AnswerServiceImpl(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    @Override
    public List<Answer> getAllAnswers() {
        return answerRepository.findAllAndFetch();
    }

    @Override
    public List<Answer> findByUserId(int userId) {
        return answerRepository.findByUserId(userId);
    }

    @Override
    public void save(List<RawAnswer> rawAnswers, User user) {
        List<Answer> answers = rawAnswers.stream().map(e -> {
            Question question = new Question(e.getQuestionId());
            return new Answer(e.getAnswer(), question, user);
        }).collect(Collectors.toList());
        answerRepository.saveAll(answers);
    }
}
