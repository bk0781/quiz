package com.my.quiz.service;

import com.my.quiz.dto.RawAnswer;
import com.my.quiz.entities.*;

import java.util.List;

public interface AnswerService {
    List<Answer> getAllAnswers();
    List<Answer> findByUserId(int userId);
    void save(List<RawAnswer> rawAnswers, User user);
}
