package com.my.quiz.service;

import com.my.quiz.entities.Category;
import com.my.quiz.repository.CategoryRepository;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category save(String name) {
        return categoryRepository.save(new Category(name));
    }

    @Override
    @Transactional
    public List<Category> getAllCategories() {
        List<Category> categories = categoryRepository.findAll();
        Hibernate.initialize(categories);
        return categories;
    }

    @Override
    public Category findFirstByName(String name) {
        return categoryRepository.findFirstByNameIgnoreCase(name);
    }

    @Override
    public Category setName(int categoryId, String name) {
        Category category = categoryRepository.findById(categoryId);
        category.setName(name);
        return category;
    }

    @Override
    public Category findById(int categoryId) {
        return categoryRepository.findById(categoryId);
    }
}
