package com.my.quiz.service;

import com.my.quiz.entities.User;
import com.my.quiz.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User save(String name, String studyGroup) {
       User user = new User(name, studyGroup);
       return userRepository.save(user);
    }

    @Override
    public User findById(int id) {
        return userRepository.findById(id);
    }

    @Override
    public User findFirstByNameAndStudyGroup(String name, String studyGroup) {
        User user = userRepository.findFirsByNameAndStudyGroup(name, studyGroup);
        return user;
    }
}
