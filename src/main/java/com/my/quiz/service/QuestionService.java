package com.my.quiz.service;

import com.my.quiz.entities.Category;
import com.my.quiz.entities.Question;

import java.util.List;

public interface QuestionService {
    List<Question> getAllQuestions();
    List<Question> getAllActiveQuestions();
    Question findById(int questionId);
    void save(String text, Category category);
    List<Question> findByCategoryId(int categoryId);
    List<Question> findByCategoryIdAndActiveTrue(int categoryId);
    Question setActiveStatus (int questionId, boolean isActive);
}
