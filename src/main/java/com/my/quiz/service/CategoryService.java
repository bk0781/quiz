package com.my.quiz.service;

import com.my.quiz.entities.Category;

import java.util.List;

public interface CategoryService {
    Category save(String name);
    List<Category> getAllCategories();
    Category findFirstByName(String name);
    Category setName(int categoryId, String name);
    Category findById (int categoryId);
}
