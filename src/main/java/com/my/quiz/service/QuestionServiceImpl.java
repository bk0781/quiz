package com.my.quiz.service;

import com.my.quiz.entities.Category;
import com.my.quiz.entities.Question;
import com.my.quiz.repository.QuestionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    private final QuestionRepository questionRepository;

    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public List<Question> getAllQuestions() {
        return questionRepository.findAll();
    }

    @Override
    public List<Question> getAllActiveQuestions() {
        return questionRepository.findByActiveTrue();
    }

    @Override
    public Question findById(int questionId) {
        return questionRepository.findById(questionId);
    }

    @Override
    public void save(String text, Category category) {
        questionRepository.save(new Question(text, category));
    }

    @Override
    public List<Question> findByCategoryId(int categoryId) {
        return questionRepository.findByCategoryId(categoryId);
    }

    @Override
    public List<Question> findByCategoryIdAndActiveTrue(int categoryId) {
        return questionRepository.findByCategoryIdAndActiveTrue(categoryId);
    }

    @Override
    public Question setActiveStatus(int questionId, boolean isActive) {
        Question question = questionRepository.findById(questionId);
        question.setActive(isActive);
        return null;
    }
}
