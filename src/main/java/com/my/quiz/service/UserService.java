package com.my.quiz.service;

import com.my.quiz.entities.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    User save(String name, String studyGroup);
    User findById(int id);
    User findFirstByNameAndStudyGroup(String name, String studyGroup);
}
